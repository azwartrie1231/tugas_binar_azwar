const express = require("express");
const jwt = require("jsonwebtoken");
const isAuthenticated = require("./is_authenticated");
const app = express();

app.use(express.json());

const users = [
  {
    id: 1,
    username: "test",
    password: "test123",
    fullName: "Binarian 1",
  },
  {
    id: 2,
    username: "test2",
    password: "test1234",
    fullName: "Binarian 2",
  },
];

const blogposts = [
  {
    id: 1,
    user_id: "1",
    title: "title1",
    content: "content1",
    likes: "1",
  },
];

app.get("/my-profile", isAuthenticated, (req, res) => {
  return res.json({
    user: req.userData.name,
  });
});

app.post("/user/register", (req, res) => {
  const { username, password, fullName } = req.body;
  const id = users[users.length - 1].id + 1;
  const user = { id, username, password, fullName };
  users.push(user);

  res.status(201).json(user);
});

app.post("/user/login", (req, res) => {
  const { username, password } = req.body;

  const isFoundUser = users.find((user) => user.username == username);

  if (isFoundUser) {
    const isValidPassword = isFoundUser.password == password;

    if (isValidPassword) {
      const jwtPayload = jwt.sign(
        { name: isFoundUser.fullName, id: isFoundUser.id },
        "SomeSecret"
      );
      return res.json({ token: jwtPayload, message: "Login Berhasil" });
    }
  }

  return res.status(400).json({
    error: true,
    message:
      "Login gagal, sepertinya user tidak terdaftar atau password anda salah",
  });
});

app.post("/blogpost", isAuthenticated, (req, res) => {
  const currentBlogpostId =
    (blogposts.length && blogposts[blogposts.length - 1].id) || 0;
  let blogpostId = currentBlogpostId + 1;

  //cektitle tidak boleh duplikat
  const { title, content, likes } = req.body;
  const cekTitle = blogposts.find((blogpost) => blogpost.title == title);
  const cekContent = blogpost.find((blogpost) => blogpost.content != null);
  console.log(cekTitle);
  console.log(cekContent);
  if (cekTitle) {
    return res.send("Title sudah ada");
  }

  const blogpost = {
    id: blogpostId,
    name: req.userData.name,
    title,
    content,
    likes,
  };

  blogposts.push(blogpost);

  return res.send({
    id: blogpostId,
    message: "blogpost berhasil ditambahkan",
    data: blogposts,
  });
});

app.get("/blogpost", (req, res) => {
  return res.send(blogposts);
});

app.listen(3000, () => console.log("Server run on port 3000"));
